function thankYscroll() {
  if (document.querySelector(".content").classList.contains("scroll")) {
    //alert("scroll is active");
    document.querySelector('.scroll').scrollIntoView({
      block: 'center',
      behavior: 'smooth'
    });
  }
}

$(document).ready(function () {
  reorderItems();
});
$(window).resize(function () {
  reorderItems()
});
$(document).scroll(function () {
  collapseMenu();
});

function reorderItems() {
  var windowW = $(window).width() + 15;

  if (windowW < 768) {
    $('.one-1').insertAfter('.three-3');
    $('.two-2').insertAfter('.four-4');
  } else {
    $('.one-1').appendTo('.zero-0');
    $('.two-2').insertAfter('.one-1');
  }
}

function collapseMenu() {
  var scroll = $(window).scrollTop();
  if(scroll > 50) {
    $('.head').addClass('head--scroll');
  } 
  if(scroll == 0) {
    $('.head').removeClass('head--scroll');
  }
}